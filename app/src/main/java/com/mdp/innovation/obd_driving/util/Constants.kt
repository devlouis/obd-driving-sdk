package com.mdp.innovation.obd_driving.util

import android.view.MotionEvent
import android.view.View

class Constants {

    companion object {
        val STARTFOREGROUND_ACTION  = "com.mdp.innovation.obd_driving.action.startforeground"
        val STOPFOREGROUND_ACTION  = "com.mdp.innovation.obd_driving.action.stopforeground"
        val ET_ACELERATION = 1
        val ET_BRAKING = 2
        val ET_TAKING_CURVES = 3
        val ET_SPEEDING = 4
        val ET_DURATION = 5
    }



}