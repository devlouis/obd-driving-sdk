package com.mdp.innovation.obd_driving.service.model

data class MyTripsRequest(val VIN: String, val page: Int, val elements: Int)